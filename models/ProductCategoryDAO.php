<?php

class ProductCategoryDAO{
	private $db;

	//============================================
	// Constructor of ProductCategoryDAO;
	//============================================
	public function __construct(DBase $db){
		$this->db = $db;
	}

	public function add(ProductCategory $productCategory){
		$stmt = $this->db->getConnection()->prepare(
			"INSERT INTO product_category (
				product_id,
				category_id
			)VALUES('" .
				$productCategory->getProductId() . "', '" .
				$productCategory->getCategoryId() .
			"')"
		);

		$stmt->execute();

		return $this->db->getConnection()->lastInsertId();
	}
}

?>