<?php

class Category{
	private $code;
	private $description;

	//============================================
	// Get/Set - $code;
	//============================================
	public function setCode($code){
		$this->code = $code;
	}

	public function getCode(){
		return $this->code;
	}

	//============================================
	// Get/Set - $description;
	//============================================
	public function setDescription($description){
		$this->description = $description;
	}

	public function getDescription(){
		return $this->description;
	}
}

?>