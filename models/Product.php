<?php

class Product{
	private $id;
	private $name;
	private $skuCode;
	private $price;
	private $description;
	private $quantity;

	//============================================
	// Get/Set - $id;
	//============================================
	public function setId($id){
		$this->id = $id;
	}

	public function getId(){
		return $this->id;
	}

	//============================================
	// Get/Set - $name;
	//============================================
	public function setName($name){
		$this->name = $name;
	}

	public function getName(){
		return $this->name;
	}

	//============================================
	// Get/Set - $skuCode;
	//============================================
	public function setSkuCode($skuCode){
		$this->skuCode = $skuCode;
	}

	public function getSkuCode(){
		return $this->skuCode;
	}

	//============================================
	// Get/Set - $price;
	//============================================
	public function setPrice($price){
		$this->price = $price;
	}

	public function getPrice(){
		return $this->price;
	}

	//============================================
	// Get/Set - $description;
	//============================================
	public function setDescription($description){
		$this->description = $description;
	}

	public function getDescription(){
		return $this->description;
	}

	//============================================
	// Get/Set - $quantity;
	//============================================
	public function setQuantity($quantity){
		$this->quantity = $quantity;
	}

	public function getQuantity(){
		return $this->quantity;
	}	
}

?>