<?php

class ProductCategory{
	private $productId;
	private $categoryId;

	//============================================
	// Get/Set - $productId;
	//============================================
	public function setproductId($productId){
		$this->productId = $productId;
	}

	public function getProductId(){
		return $this->productId;
	}

	//============================================
	// Get/Set - $categoryId;
	//============================================
	public function setCategoryId($categoryId){
		$this->categoryId = $categoryId;
	}

	public function getCategoryId(){
		return $this->categoryId;
	}
}

?>