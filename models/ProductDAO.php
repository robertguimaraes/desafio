<?php

class ProductDAO{
	private $db;

	//============================================
	// Constructor of ProductDAO;
	//============================================
	public function __construct(DBase $db){
		$this->db = $db;
	}

	public function add(Product $product){
		$stmt = $this->db->getConnection()->prepare(
			"INSERT INTO product (
				name,
				skuCode,
				description,
				quantity,
				price
			) VALUES('" .
				$product->getName() . "', '" .
				$product->getSkuCode() . "', '" .
				$product->getDescription() . "', " .
				$product->getQuantity() . ", " .
				$product->getPrice() .
			")"
		);

		$stmt->execute();

		return $this->db->getConnection()->lastInsertId();
	}

	public function select(){
		$stmt = $this->db->getConnection()->query(
			"SELECT
				product_id,
				name,
				skuCode,
				price,
				description,
				quantity
			  FROM product"
		);

		return $stmt->fetchAll();
	}
}

?>