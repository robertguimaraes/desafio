<?php

class CategoryDAO{
	private $db;

	//============================================
	// Constructor of CategoryDAO;
	//============================================
	public function __construct(DBase $db){
		$this->db = $db;
	}

	public function add(Category $category){
		// Select para validar se a categoria já existe no banco.
		$stmt = $this->db->getConnection()->query(
			"SELECT
			    category_id,
			    code,
			    description
			  FROM category
			  WHERE
			    code = '" . $category->getCode() . "' AND
			    description = '" . $category->getDescription() . "'"
		);

		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// Efetua a inserção, para o caso do select não retornar registros.
		if (!$row) {
			$stmt = $this->db->getConnection()->prepare(
				"INSERT INTO category (
					code,
					description
				)VALUES('" .
					$category->getCode() . "', '" .
					$category->getDescription() .
				"')"
			);

			$stmt->execute();

			return $this->db->getConnection()->lastInsertId();
		} else {
			return $row['category_id'];
		}
	}

	public function select(){
		$stmt = $this->db->getConnection()->query("SELECT category_id, code, description FROM category");

		return $stmt->fetchAll();
	}
}

?>