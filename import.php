<?php

require ("/controllers/CategoryController.php");
require ("/controllers/ProductController.php");
require ("/controllers/ProductCategoryController.php");

function importCSV() {
	$arrCategories = array();
	$productId;

	$filename = "import.csv";
	$file = fopen($filename, "r");

	//Contador de linhas.
	$aux = file('import.csv');
	$totalRows = count($aux) - 1; // -1 pelo header.
	$currentRow = 0;

	ob_implicit_flush(true);

	while(ob_get_level() > 0) ob_end_clean();

	echo "#######################<br>";
	echo "Importação iniciada... <br>";
	echo "#######################<br>";

	$flag = true;
	while (($column = fgetcsv($file, 10000, ";")) !== FALSE) {
		// Pula a primeira linha (header).
		if($flag) {
			$flag = false;
			continue;
		}

		$currentRow++;
		
		$name = $column[0];
		$skuCode = $column[1];
		$description = $column[2];
		$quantity = $column[3];
		$price = $column[4];
		$category = $column[5];

		// Importa as categorias, sem permitir que elas se repitam (DAO).
		if (!empty($category)) {
			$categoryController = new CategoryController();
			
			unset($arrCategories);
			$arrCategories = array();

			foreach (explode('|', $category) as $c) {
				$catCode = strtoupper(substr($c, 0, 3));
				$res = $categoryController->insertCategory($catCode, $c);
				$arrCategories[] = $res;
			}

			$categoryController = null;
		}

		// Importa o produto.
		$productController = new ProductController();
		
		$productId = $productController->insertProduct(
			$name,
			$skuCode,
			$description,
			$quantity,
			$price
		);

		$productController = null;

		// Cria o vínculo entre o produto e suas categorias.
		$productCategoryController = new ProductCategoryController();

		foreach ($arrCategories as $category) {
			$productCategoryController->insertProductCategory(
				$productId,
				$category
			);
			// echo "Produto " . $productId . " - Categoria " . $category . "<br>";
		}

		$productCategoryController = null;

		echo "[" . $currentRow . " of " . $totalRows . "] - Produto (" . $skuCode . ") importado com sucesso! ID do MySQL = " . $res . " <br>";
	}

	echo "##################################<br>";
	echo "Importação finalizada com sucesso!<br>";
	echo "##################################<br>";
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Importação CSV | WEBJUMP</title>
</head>

<body>
	<textarea style="width: 700px;height: 880px;"><?php importCSV() ?></textarea>
</body>

</html> 