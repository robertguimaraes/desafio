<?php

ini_set('max_execution_time', 300);

class DBase{
	private $dsn = "mysql:host=127.0.0.1;dbname=WEBJUMP";
	private $user = "root";
	private $pass = "";
	private $connection = null; 

	//======================================
	// Constructor of DBase class.
	//======================================
	public function __construct(){
		$this->conect();
	}

	//======================================
	// Get of $connection property.
	//======================================
	public function getConnection(){
		return $this->connection;
	}

	public function lastId(){
		$this->connection->lastInsertId();
	}

	//======================================
    // Conect method.
	//======================================
    private function conect(){
		$this->connection = new PDO($this->dsn, $this->user, $this->pass);
    }
}

?>