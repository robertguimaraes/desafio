<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . '/models/ProductCategory.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/models/ProductCategoryDAO.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/config/dbase.php');

class ProductCategoryController{

	public function insertProductCategory($productId, $categoryId) {
		$db = new DBase();
		$dao = new ProductCategoryDAO($db);

		$productCategory = new ProductCategory();
		$productCategory->setproductId($productId);
		$productCategory->setCategoryId($categoryId);

		return $dao->add($productCategory);

		$db = null;
		$dao = null;
		$productCategory = null;
	}
}

?>