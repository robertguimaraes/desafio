<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . '/models/Category.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/models/CategoryDAO.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/config/dbase.php');

class CategoryController{

	public function insertCategory($code, $name) {
		$db = new DBase();
		$dao = new CategoryDAO($db);

		$category = new Category();
		$category->setCode($code);
		$category->setDescription($name);

		return $dao->add($category);

		$db = null;
		$dao = null;
		$category = null;
	}

	public function listCategories(){
		$db = new DBase();
		$dao = new CategoryDAO($db);

		$res = $dao->select();

		foreach ($res as $row) {
			echo '<tr class="data-row">
			        <td class="data-grid-td">
			           <span class="data-grid-cell-content">' . $row["category_id"] . '</span>
			        </td>
			      
			        <td class="data-grid-td">
			           <span class="data-grid-cell-content">' . $row["description"] . '</span>
			        </td>
			      
			        <td class="data-grid-td">
			          <div class="actions">
			          <div class="action edit"><span >Edit</span></div>
					  <form action="/controllers/CategoryController.php" method="post">
					    <div class="action delete"><button name="delete" type="submit" id="delete" value="Delete">Delete</button></div>
					  </form>
			          </div>
			        </td>




			      </tr>';			
		}
	}

	public function insertCategory2($name, $code){
		$db = new DBase();
		$dao = new CategoryDAO($db);

		$category = new Category();
		$category->setDescription($name);

		$dao->add($category);

		header("Location: ../categories.php");
		die();
	}
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	if (!empty($_POST['cat-name']) && !empty($_POST['cat-code'])){
		$catController = new CategoryController();

		$catController->insertCategory($_POST['cat-name'], $_POST['cat-code']);
	} else {
		echo "Por favor, preencha o nome e o código da categoria!" .
			 "<br><br><button type='button' onclick='history.back();'>Voltar</button>";
	}
}

?>