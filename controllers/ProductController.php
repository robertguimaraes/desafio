<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . '/models/Product.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/models/ProductDAO.php');
require_once ($_SERVER['DOCUMENT_ROOT'] . '/config/dbase.php');

class ProductController{

	public function insertProduct($name, $skuCode, $description, $quantity, $price) {
		$db = new DBase();
		$dao = new ProductDAO($db);

		$product = new Product();
		$product->setName($name);
		$product->setSkuCode($skuCode);
		$product->setDescription($description);
		$product->setQuantity($quantity);
		$product->setPrice($price);

		return $dao->add($product);

		$db = null;
		$dao = null;
		$product = null;
	}
}

?>