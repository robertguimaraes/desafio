# Teste WEBJUMP - Importação de arquivo CSV.

Esta fase contempla a importação do arquivo CSV, considerando a tabela de produtos (product), categorias (category) e vínculo entre PRODUTOS x CATEGORIAS (product_category).

Basta executar o arquivo "import.php", e as seguintes ações serão executadas ao ler o arquivo "import.csv" (no mesmo diretório):

1. Insere, sem duplicar registros, as **categorias** do produto vigente no loop, na tabela "category";
2. Insere o **produto** na tabela "product";
3. Insere os vínculos entre os **produtos e suas respectivas categorias** na tabela "product_category".

As informações são importadas para um banco **MySQL**, do qual há a exportação da estrutura no arquivo "webjump.sql".

Mesmo que meu teste WEBJUMP não esteja completo, com a importação CSV temos todas as classes prontas (classes de produtos/categorias, com seus respectivos "DAO", e "controllers").
Tais classes podem ser utilizadas para a implementação do CRUD nos arquivos disponibilizados pela WEBJUMP de forma fácil, uma vez que o "grosso" já foi feito.

Acredito que, mesmo que não esteja completo, pode ser algo a ser considerado para uma pequena avaliação.